/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.oxgui;

import java.util.Scanner;

/**
 *
 * @author bwstx
 */
public class Game {

    private Player o;
    private Player x;
    private int row;
    private int col;
    private Board board;
    private char reset;
    private boolean newGame = false;
    Scanner kb = new Scanner(System.in);

    public Game() {
        this.o = new Player('O');
        this.x = new Player('X');
    }

    public void newBoard() {
        this.board = new Board(o, x);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTurn() {
        Player player = board.getCurrentPlayer();
        System.out.println("Turn " + player.getSymbol());
    }

    public void showTable() {
        char[][] table = this.board.getTable();
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    public void inputRowCol() {
        while (true) {
            System.out.println("Please input row, col: ");
            row = kb.nextInt();
            col = kb.nextInt();
            if (board.setRowCol(row, col)) {
                return;
            }
        }
    }

    public boolean isFinish() {
        if (board.isDraw() || board.isWin()) {
            return true;
        }
        return false;

    }

    public boolean doContinue() {
            System.out.println("Play again? (y/n):");
            reset = kb.next().charAt(0);
            if (reset == 'y') {
                return true;
            } else if (reset == 'n') {
                return false;
            }
        return true;
    }

    public void showStat() {
        System.out.println(o);
        System.out.println(x);
    }

    public void showResult() {
        if (board.isDraw()) {
            System.out.println("Draw!!");
        } else if (board.isWin()) {
            System.out.println(board.getCurrentPlayer().getSymbol() + " Win!!");
        }
    }

}
